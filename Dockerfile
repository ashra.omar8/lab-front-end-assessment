# Use the official Docker Library Drupal image
php:7.2.5-apache

# It contains Apache - the web server - and PHP.
# Both Apache and PHP are already setup for Drupal sites.

# Download and install needed CLI tools
RUN apt-get update && apt-get install -y \
  curl \
  git \
  wget

# Remove the provided Drupal files
RUN rm -rf /var/www/html/*

# # Copy project's Apache VirtualHost configuration file
# COPY apache-drupal.conf /etc/apache2/sites-enabled/000-default.conf
