<footer class="bck-red">
	<div class="" role="presentation">
			<nav class="menu-footer ">
				<div class="site-links">
					<ul class="non-list">
						<li>
							<a href="/">Home</a>
						</li>
						<li>
							<a href="/products.php">Products</a>
						</li><!--
						<li>
							<a href="/history.php">History</a>
						</li> -->
						<li>
							<a href="/contact.php">Contact us</a>
						</li>
					</ul>
				</div>
			</nav>
			<div>
				<p><a target="_blank" href="https://www.google.com/maps?ll=-33.859904,18.691036&z=14&t=m&hl=en-GB&gl=US&mapclient=embed&q=Unit+6,+2+Kiewiet+Close,+Okavango+Park+1,+Brackenfell,+Cape+Town,+7560,+South+Africa">
					Unit 6, 2 Kiewiet Close, Okavango Park 1, Brackenfell, Cape Town, 7560, South Africa</a></p>
			</div>
			<div class="social-media">
				<!-- LinkedIn -->
				<a href="https://www.linkedin.com/company/taurus-doors/" target="_blank">
					<svg aria-hidden="true" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin fa-w-14 fa-3x"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" class=""></path></svg>
				</a>
				<!-- Facebook -->
				<!-- <a href="https://www.facebook.com/pages/Taurus-Automatic-Doors/1143879722318757" target="_blank"> -->
				<a href="https://www.facebook.com/taurusdoors/" target="_blank">
					<svg aria-hidden="true" data-prefix="fab" data-icon="facebook-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-facebook-square fa-w-14 fa-3x"><path fill="currentColor" d="M448 80v352c0 26.5-21.5 48-48 48h-85.3V302.8h60.6l8.7-67.6h-69.3V192c0-19.6 5.4-32.9 33.5-32.9H384V98.7c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9H184v67.6h60.9V480H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48z" class=""></path></svg>
				</a>
			</div>
	</div>
</footer>
