
/**
 * @file JaveScript
 */

/*
 * CONTENTS
 */
$(function() {
  set_up_burger_menu();
  setup_carousel_slide();
  setup_big_banner_slide();
  set_up_scroll();
  setup_form_fields();
  set_up_form_log_data();
});



// Tablate and phone menu 
function set_up_burger_menu() {

 $(".hamburger" ).click(function() {
  $(this).children("p").toggleClass('show');
  console.log('text');
    $("nav[role='navigation']").toggleClass("show");
    // $(".burger").toggleClass("open");

    return false;
  });
}


//Carousels (Banner slider)
function setup_big_banner_slide()  {

  $('.carousel').slick({
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
    infinite: false,
    autoplay: false,
    dots: true,
  });
}

//Footer section carousel
function setup_carousel_slide()  {
  
  let next_button = "<button type='button' class='slick-next pull-right'><i class='material-icons'>keyboard_arrow_right</i></button>";
  let prev_button = "<button type='button' class='slick-prev pull-left'><i class='material-icons'>keyboard_arrow_left</i></button>";

  $('.carousel-small').slick({
    slidesToShow: 6,
    arrows: true,
    slidesToScroll: 1,
    infinite: false,
    autoplay: false,
    nextArrow: next_button,
    prevArrow: prev_button,
    responsive: [

      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
}

// Move label out of input 
function setup_form_fields() {
  $("form").on('focusin', 'input', function() {
    $(this).parents('.form-item').addClass('focussed');
    // console.log('test');
  })
  .on('focusout', 'input, textarea', function() {
    $(this).parents('.form-item').removeClass('focussed');
  })
  .on('change', 'input, textarea', function() {
    let val = $(this).val();
    if (val) {
      $(this).parents('.form-item').addClass('has-content');
    }
    else {
      $(this).parents('.form-item').removeClass('has-content');
    }
  });
}



// Scroll slide range number
function set_up_scroll() {
  $( "#numberRange" ).change(function() {

    //pass value to Howhapyy input
    let rangesValues = $( "#numberRange" ).val();
    document.getElementById('howHappy').value = rangesValues;
      // console.log(rangesValues);

      //Change image if math numer
      if(rangesValues <=2 ) {
        $('.very-happy').removeClass( "hidden" ).addClass( "show" );

        $('.happy').removeClass( "show" ).addClass( "hidden" );

        $('.sad').addClass( "hidden" ).removeClass( "show" );
      }
       
      else if (rangesValues ==3 || rangesValues ==4 ) {
        $('.happy').removeClass( "hidden" ).addClass( "show" );

        $('.very-happy').removeClass( "show" ).addClass( "hidden" );

        $('.sad').addClass( "hidden" ).removeClass( "show" );
      }
      else if (rangesValues =5) {
        $('.sad').removeClass( "hidden" ).addClass( "show" );

        $('.very-happy').removeClass( "show" ).addClass( "hidden" );
        
        $('.happy').removeClass( "show" ).addClass( "hidden" );
      }
  });
}


function set_up_form_log_data() {

  document.contactForm.reset();

  $("form").submit(function(e){
     e.preventDefault();

    const name = document.getElementById('fullName').value;
    const email = document.getElementById('email').value;
    const contactNumber = document.getElementById('contactNumber').value;
    const howHappy = document.getElementById('howHappy').value;
    const conceptName = $('#select').find(":selected").text();

    function happinessIndicator() {
      if(howHappy <= 2) {
         console.log(`${name} is happy`);
      }
      else if(howHappy ==3 || howHappy ==4) {
         console.log(`${name} is sad`);  
      }
      else { 
        console.log(`${name} is very sad`);
      }
    }

    happinessIndicator();

    // let FllName = name.value;
    let elementValue = [
    `full Name: ${name}`,
    `Email: ${email}`,
    `Contact number: ${contactNumber}`,
    `${conceptName}`
    ];
    console.log(elementValue);
  });
  
  // // const isValidEmail= email.checkValidity();

  // // form validation 
  // //Email Name 
  // function formValidation() {
  //   if (email.checkValidity()) {
  //   alert("Please enter a valid email");
  //   return false;
  //   }
  // }
}